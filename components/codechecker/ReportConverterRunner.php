<?php

namespace app\components\codechecker;

use app\components\docker\DockerContainer;
use app\components\docker\DockerContainerBuilder;
use app\components\docker\EvaluatorTarBuilder;
use app\exceptions\CodeCheckerRunnerException;
use app\models\Submission;
use Yii;

/**
 * Runs a static analyzer tool in the configured environment then creates CodeChecker reports from it.
 */
class ReportConverterRunner extends AnalyzerRunner
{
    public function __construct(Submission $submission)
    {
        parent::__construct($submission);
    }

    protected function beforeRun(): void
    {
        parent::beforeRun();
        $os = $this->submission->task->testOS;
        $imageName = Yii::$app->params["evaluator"]["reportConverterImage"][$os];

        if (empty($imageName)) {
            throw new CodeCheckerRunnerException(
                Yii::t("app", "CodeChecker Report Converter Docker image is not set in params.php"),
                CodeCheckerRunnerException::BEFORE_RUN_FAILURE
            );
        }
    }

    protected function addAnalyzeInstructionsToTar(EvaluatorTarBuilder $tarBuilder): void
    {
        $tarBuilder->withTextFile(
            "analyze" . ($this->submission->task->testOS == 'windows' ? '.ps1' : '.sh'),
            $this->submission->task->staticCodeAnalyzerInstructions
        );
    }

    /**
     * @param DockerContainer $analyzerContainer
     * @return string|null
     * @throws CodeCheckerRunnerException
     */
    protected function createAndDownloadReportsTar(DockerContainer $analyzerContainer): ?string
    {
        if (!$this->checkIfReportsArePresent($analyzerContainer)) {
            return null;
        }

        $reportConverterContainer = $this->buildAndStartReportConverterContainer();
        try {
            $this->copyTestDirectoryToConverterContainer($analyzerContainer, $reportConverterContainer);
            $this->runReportConverter($reportConverterContainer);
            $this->runParseCommand($reportConverterContainer);
            return $this->downloadReportsFromReportConverterContainer($reportConverterContainer);
        } finally {
            $reportConverterContainer->stopContainer();
        }
    }

    /**
     * Checks if the output of the current tool exists in the analyzer container
     * @param DockerContainer $analyzerContainer
     * @return bool
     * @throws CodeCheckerRunnerException
     */
    private function checkIfReportsArePresent(DockerContainer $analyzerContainer): bool
    {
        $supportedTools = Yii::$app->params["evaluator"]["supportedStaticAnalyzerTools"];
        $toolName = $this->submission->task->staticCodeAnalyzerTool;
        $outputPath = $supportedTools[$toolName]["outputPath"];
        if (empty($outputPath)) {
            throw new CodeCheckerRunnerException(
                Yii::t("app", "'outputPath' is not provided in params.php for the {toolName} tool", ["toolName" => $toolName])
            );
        }

        if ($this->submission->task->testOS == 'windows') {
            $outputPath = str_replace('/', '\\', $outputPath);
            $result = $analyzerContainer->executeCommand(
                ["powershell", "-Command", "Test-Path -Path C:\\test\\$outputPath"]
            );
            return rtrim($result["stdout"]) === "True";
        } else {
            $result = $analyzerContainer->executeCommand(["test", "-e", "/test/$outputPath"]);
            return $result["exitCode"] === 0;
        }
    }

    /**
     * Builds the Docker container that contains the report-converter tool.
     * If the needed image is not present then tries to pull it.
     * @return DockerContainer
     * @throws CodeCheckerRunnerException
     */
    protected function buildAndStartReportConverterContainer(): DockerContainer
    {
        try {
            $os = $this->submission->task->testOS;
            $imageName = Yii::$app->params["evaluator"]["reportConverterImage"][$os];

            $builder = new DockerContainerBuilder($os, $imageName);
            $container = $builder->build("tms_report_converter_" . $this->submission->id);
            $container->startContainer();
            return $container;
        } catch (\Throwable $e) {
            throw new CodeCheckerRunnerException(
                Yii::t('app', 'Failed to create or start Docker container'),
                CodeCheckerRunnerException::PARSE_FAILURE,
                null,
                $e
            );
        }
    }

    /**
     * Copies the contents of the test directory to the container that contains report-converter.
     * @param DockerContainer $analyzerContainer
     * @param DockerContainer $reportConverterContainer
     * @return void
     * @throws CodeCheckerRunnerException
     */
    private function copyTestDirectoryToConverterContainer(
        DockerContainer $analyzerContainer,
        DockerContainer $reportConverterContainer
    ) {
        try {
            $tarPath = $this->workingDirBasePath . "/analyzed_test.tar";
            $analyzerContainer->downloadArchive(
                $this->submission->task->testOS === "linux" ? "/test" : "C:\\test",
                $tarPath
            );
            $reportConverterContainer->uploadArchive($tarPath, '/');
        } catch (\Throwable $e) {
            throw new CodeCheckerRunnerException(
                Yii::t('app', 'Failed to copy project files to the Report Converter container'),
                CodeCheckerRunnerException::PARSE_FAILURE,
                null,
                $e
            );
        }
    }

    /**
     * Runs report converter on the report-converter container
     * @param DockerContainer $container
     * @return void
     * @throws CodeCheckerRunnerException
     */
    private function runReportConverter(DockerContainer $container)
    {
        try {
            $toolName = $this->submission->task->staticCodeAnalyzerTool;
            $resultFilePath = ($this->submission->task->testOS === "windows" ? "C:\\test\\" : "/test/")
                . Yii::$app->params["evaluator"]["supportedStaticAnalyzerTools"][$toolName]["outputPath"];
            $plistPath = $this->submission->task->testOS === "windows" ? "C:\\test\\reports\\plist" : "/test/reports/plist";

            $container->executeCommand(
                [
                   "report-converter",
                   "-t", $this->submission->task->staticCodeAnalyzerTool,
                   "-o", $plistPath,
                   $resultFilePath
                ]
            );
        } catch (\Throwable $e) {
            throw new CodeCheckerRunnerException(
                Yii::t('app', 'Failed to run the Report Converter tool'),
                CodeCheckerRunnerException::PARSE_FAILURE,
                null,
                $e
            );
        }
    }

    /**
     * Downloads converted reports from the Report Converter container
     * @param DockerContainer $reportConverterContainer
     * @return string The location of the downloaded tar file
     * @throws CodeCheckerRunnerException
     */
    private function downloadReportsFromReportConverterContainer(DockerContainer $reportConverterContainer): string
    {
        try {
            $tarPath = $this->workingDirBasePath . "/reports.tar";
            $reportConverterContainer->downloadArchive(
                $this->submission->task->testOS  === "windows"
                    ? "C:\\test\\reports" : "/test/reports",
                $tarPath
            );
            return $tarPath;
        } catch (\Throwable $e) {
            throw new CodeCheckerRunnerException(
                Yii::t('app', 'Failed to download reports from the Report Converter container'),
                CodeCheckerRunnerException::PARSE_FAILURE,
                null,
                $e
            );
        }
    }
}
