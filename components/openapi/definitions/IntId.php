<?php

namespace app\components\openapi\definitions;

/**
 * @OA\Schema(
 *   schema="int_id",
 *   type="integer",
 *   format="int64",
 *   minimum=1,
 * )
 */
abstract class IntId
{
}
