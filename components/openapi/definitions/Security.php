<?php

namespace app\components\openapi\definitions;

/**
 * @OA\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      type="http",
 *      scheme="bearer",
 * )
 */
abstract class Security
{
}
