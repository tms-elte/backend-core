<?php

return [
    'testresult1' => [
        'id' => 1,
        'testCaseID' => 5,
        'submissionID' => 51,
        'isPassed' => 0,
        'errorMsg' => 'FULL_ERROR_MESSAGE'
    ],
    'testresult2' => [
        'id' => 2,
        'testCaseID' => 6,
        'submissionID' => 51,
        'isPassed' => 1,
        'errorMsg' => null
    ],
    'testresult3' => [
        'id' => 3,
        'testCaseID' => 7,
        'submissionID' => 52,
        'isPassed' => 0,
        'errorMsg' => 'FULL_ERROR_MESSAGE'
    ],
    'testresult4' => [
        'id' => 4,
        'testCaseID' => 8,
        'submissionID' => 52,
        'isPassed' => 1,
        'errorMsg' => null
    ],
];
