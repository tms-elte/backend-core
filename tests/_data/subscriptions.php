<?php

return [
    'subscription1' => [
        'id' => 1,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2000,
        'semesterID' => 3001,
        'notes' => 'subscription1'
    ],
    'subscription2' => [
        'id' => 2,
        'userID' => 1002,
        'isAccepted' => 1,
        'groupID' => 2000,
        'semesterID' => 3001,
        'notes' => 'subscription2'
    ],
    'subscription3' => [
        'id' => 3,
        'userID' => 1003,
        'isAccepted' => 1,
        'groupID' => 2000,
        'semesterID' => 3001,
        'notes' => 'subscription3'
    ],
    'subscription4' => [
        'id' => 4,
        'userID' => 1002,
        'isAccepted' => 1,
        'groupID' => 2007,
        'semesterID' => 3001,
        'notes' => 'subscription4'
    ],
    'subscription5' => [
        'id' => 5,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2001,
        'semesterID' => 3001,
        'notes' => 'subscription5'
    ],
    'subscription6' => [
        'id' => 6,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2005,
        'semesterID' => 3001,
        'notes' => 'subscription6'
    ],
    'subscription7' => [
        'id' => 7,
        'userID' => 1001,
        'isAccepted' => 1,
        'groupID' => 2010,
        'semesterID' => 3000,
        'notes' => 'subscription7'
    ],
    'subscription8' => [
        'id' => 8,
        'userID' => 1011,
        'isAccepted' => 0,
        'groupID' => 2000,
        'semesterID' => 3001,
        'notes' => 'subscription8'
    ]
];
