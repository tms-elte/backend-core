<?php

return [
    'moss1' => [
        'id' => 7001,
        'plagiarismId' => 1,
        'response' => null,
        'ignoreThreshold' => 10,
    ],
    'moss2' => [
        'id' => 7002,
        'plagiarismId' => 2,
        'response' => null,
        'ignoreThreshold' => 10,
    ],
    'moss3' => [
        'id' => 7003,
        'plagiarismId' => 3,
        'response' => null,
        'ignoreThreshold' => 5,
    ],
    'moss4' => [
        'id' => 7004,
        'plagiarismId' => 4,
        'response' => null,
        'ignoreThreshold' => 10,
    ],
    'moss5' => [
        'id' => 7005,
        'plagiarismId' => 5,
        'response' => null,
        'ignoreThreshold' => 10,
    ],
    'moss6' => [
        'id' => 7006,
        'plagiarismId' => 6,
        'response' => "http://moss.stanford.edu/results/0/12345689\n",
        'ignoreThreshold' => 10,
    ],
    'moss7' => [
        'id' => 7007,
        'plagiarismId' => 7,
        'response' => "http://moss.stanford.edu/results/0/12345689\n",
        'ignoreThreshold' => 10,
    ],
];
