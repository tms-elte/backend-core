<?php

return [
    'question' => [
        'id' => 1,
        'text' => 'Text',
        'questionsetID' => 1,
    ],
    'question1' => [
        'id' => 2,
        'text' => 'Hello world',
        'questionsetID' => 1,
    ],
    'question2' => [
        'id' => 3,
        'text' => 'Hello world',
        'questionsetID' => 1,
    ],
    'question3' => [
        'id' => 4,
        'text' => 'Question text',
        'questionsetID' => 2,
    ],
    'question4' => [
        'id' => 5,
        'text' => 'Question text',
        'questionsetID' => 4,
    ],
    'question5' => [
        'id' => 6,
        'text' => 'Question text',
        'questionsetID' => 3,
    ]
];
