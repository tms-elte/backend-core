<?php

return [
    'users0' => [
        'item_name' => 'admin',
        'user_id' => 1000,
        'created_at' => time(),
    ],
    'users1' => [
        'item_name' => 'student',
        'user_id' => 1001,
        'created_at' => time(),
    ],
    'users2' => [
        'item_name' => 'student',
        'user_id' => 1002,
        'created_at' => time(),
    ],
    'users3' => [
        'item_name' => 'student',
        'user_id' => 1003,
        'created_at' => time(),
    ],
    'users4' => [
        'item_name' => 'student',
        'user_id' => 1004,
        'created_at' => time(),
    ],
    'users5' => [
        'item_name' => 'student',
        'user_id' => 1005,
        'created_at' => time(),
    ],
    'teach1' => [
        'item_name' => 'faculty',
        'user_id' => 1006,
        'created_at' => time(),
    ],
    'teach2' => [
        'item_name' => 'faculty',
        'user_id' => 1007,
        'created_at' => time(),
    ],
    'teach3' => [
        'item_name' => 'faculty',
        'user_id' => 1008,
        'created_at' => time(),
    ],
    'teach4' => [
        'item_name' => 'faculty',
        'user_id' => 1009,
        'created_at' => time(),
    ],
    'teach5' => [
        'item_name' => 'faculty',
        'user_id' => 1010,
        'created_at' => time(),
    ],
];
