<?php

return [
    "question" => [
        "testinstanceID" => 1,
        "questionID" => 1,
    ],
    "question2" => [
        "testinstanceID" => 2,
        "questionID" => 1,
    ],
    "question3" => [
        "testinstanceID" => 3,
        "questionID" => 1,
    ],

    "question4" => [
        "testinstanceID" => 8,
        "questionID" => 1,
    ],
    "question5" => [
        "testinstanceID" => 8,
        "questionID" => 2,
    ],
    "question6" => [
        "testinstanceID" => 8,
        "questionID" => 3,
    ],

    "question7" => [
        "testinstanceID" => 9,
        "questionID" => 1,
    ],
    "question8" => [
        "testinstanceID" => 9,
        "questionID" => 2,
    ],
    "question9" => [
        "testinstanceID" => 9,
        "questionID" => 3,
    ]
];
