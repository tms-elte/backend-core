<?php

return [
    'questionset' => [
        'id' => 1,
        'name' => 'Question set',
        'courseID' => 4000,
    ],
    'questionset2' => [
        'id' => 2,
        'name' => 'Question set 2',
        'courseID' => 4000,
    ],
    // questionset without finalized test
    'questionset3' => [
        'id' => 3,
        'name' => 'Question set 3',
        'courseID' => 4000,
    ],
    'questionset4' => [
        'id' => 4,
        'name' => 'Question set 4',
        'courseID' => 4002,
    ],
];
