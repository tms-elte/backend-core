<?php

return [
    'courseCode0' => [
        'id' => 4000,
        'courseId' => 4000,
        'code' => '1',
    ],
    'courseCode1' => [
        'id' => 4001,
        'courseId' => 4001,
        'code' => '2',
    ],
    'courseCode3' => [
        'id' => 4002,
        'courseId' => 4002,
        'code' => '3',
    ],
    'courseCode4' => [
        'id' => 4003,
        'courseId' => 4003,
        'code' => '4',
    ],
];
