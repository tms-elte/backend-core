<?php

return [
    'course0' => [
        'id' => 4000,
        'name' => 'Java',
    ],
    'course1' => [
        'id' => 4001,
        'name' => 'C++',
    ],
    'course3' => [
        'id' => 4002,
        'name' => 'C#',
    ],
    'course4' => [
        'id' => 4003,
        'name' => 'OEP',
    ],
];
