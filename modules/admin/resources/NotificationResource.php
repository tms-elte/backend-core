<?php

namespace app\modules\admin\resources;

use app\components\openapi\generators\OAProperty;
use app\models\Notification;
use yii\helpers\ArrayHelper;

/**
 * Resource class for module 'Notification'
 */
class NotificationResource extends Notification
{
    public function scenarios(): array
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_DEFAULT] = ['id', 'message', 'startTime', 'endTime', 'scope', 'dismissible'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        return [
            'id',
            'message',
            'startTime',
            'endTime',
            'scope',
            'dismissible',
        ];
    }

    /**
     * @inheritdoc
     */
    public function extraFields(): array
    {
        return [];
    }
}
