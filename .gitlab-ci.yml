# Default image (https://gitlab.com/tms-elte/docker-images/container_registry)
image: registry.gitlab.com/tms-elte/docker-images/php:7.4

# Stages: lint -> test -> docs -> deploy
stages:
  - lint
  - test
  - docs
  - deploy

# Cached files between builds
cache:
  key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
  paths:
    - vendor/

# Dependency scanning & Secret detection & SAST
include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml

.setup-nodb:
  before_script: &setup-nodb-before-script
    # Run Composer
    - composer install --prefer-dist --no-ansi --no-interaction
    # Configure application
    - cp -f config/config.ci.yml config.yml
    - cp -f config/config.ci.yml config.test.yml

.setup-db:
  extends: .setup-nodb
  before_script:
    - *setup-nodb-before-script
    # Initialize database
    - php tests/bin/yii migrate --interactive=0
 
.service-mariadb:
  variables:
    # Configure mariadb environment variables (https://hub.docker.com/_/mariadb/)
    MYSQL_HOST: mariadb
    MYSQL_USER: tms
    MYSQL_PASSWORD: tms
    MYSQL_DATABASE: tms
    MYSQL_ROOT_PASSWORD: v7tqfR9U

  # Services: https://docs.gitlab.com/ee/ci/services/
  services:
    - name: mariadb:10.3
      alias: db
    
.service-mysql:
  variables:
    # Configure mysql environment variables (https://hub.docker.com/_/mysql/)
    MYSQL_HOST: mysql
    MYSQL_USER: tms
    MYSQL_PASSWORD: tms
    MYSQL_DATABASE: tms
    MYSQL_ROOT_PASSWORD: v7tqfR9U

  # Services: https://docs.gitlab.com/ee/ci/services/
  services:
    - name: mysql:8.0.34
      alias: db

# PHPCS linter
phpcs:
  stage: lint
  extends: .setup-nodb
  script:
    - composer lint -- -n --report=full
  allow_failure: true


# PHPStan static analyzer
phpstan:
  stage: lint
  before_script:
    # Run Composer
    - composer install --prefer-dist --no-ansi --no-interaction
  script:
    - composer test:generate
    - composer analyze:phpstan -- --no-progress --error-format gitlab > phpstan.json
  allow_failure: true
  artifacts:
    reports:
      codequality: phpstan.json

# Codeception tests & coverage reports
.codecept:
  stage: test
  extends: .setup-db
  variables:
    # Enable code coverage for Xdebug
    XDEBUG_MODE: develop,coverage
  script:
    - composer test -- --no-ansi --xml --coverage --coverage-text --coverage-xml --coverage-cobertura
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'
  artifacts:
    paths:
      - tests/_output/coverage.txt
      - tests/_output/coverage.xml
      - tests/_output/cobertura.xml
      - tests/_output/report.xml
    reports:
      coverage_report:
          coverage_format: cobertura
          path: tests/_output/cobertura.xml
      junit:
          - tests/_output/report.xml
          
# PHP 7.4 with MariaDB
codecept-7.4-mariadb:
  stage: test
  extends:
    - .codecept
    - .service-mariadb
  image: registry.gitlab.com/tms-elte/docker-images/php:7.4

# PHP 7.4 with MySQL
codecept-7.4-mysql:
  stage: test
  extends:
    - .codecept
    - .service-mysql
  image: registry.gitlab.com/tms-elte/docker-images/php:7.4
  
# Generates OpenApi docs in yaml and json format then packages it with swagger-ui
openapi:
  # The schema generator requires active database connection
  extends:
    - .setup-db
    - .service-mariadb
  stage: docs
  script:
    - cp -r components/openapi/swagger-ui-ci swagger-ui
    - php yii open-api/generate-docs json > swagger-ui/openapi-docs.json
    - php yii open-api/generate-docs yaml > swagger-ui/openapi-docs.yaml
  artifacts:
    paths:
      - swagger-ui
    expire_in: 1 day

# Generates phpdoc documentation
phpdoc:
  stage: docs
  before_script:
    - 'which curl || ( apt-get update -yqq && apt-get install -yqq curl)'
  script:
    - curl -sL https://github.com/phpDocumentor/phpDocumentor/releases/download/v3.3.1/phpDocumentor.phar -o phpdoc
    - chmod +x phpdoc
    - ./phpdoc
  artifacts:
    paths:
      - .phpdoc/build
    expire_in: 1 day

# Collect generated docs and deploy them to GitLab Pages
pages:
  stage: deploy
  image: ubuntu:20.04
  script:
    # Copy all docs to the public folder
    - mkdir public
    - cp -r swagger-ui public
    - cp -r .phpdoc/build public/phpdoc
  artifacts:
    paths:
      - public
    expire_in: 1 day
  rules:
    - if: '$CI_COMMIT_REF_NAME == "develop" && $CI_PROJECT_NAMESPACE == "tms-elte"'

# Deployment over SSH
.deploy:
  stage: deploy
  image: ubuntu:20.04
  variables:
    DEPLOY_PATH: /var/www/html
  before_script:
    - 'which ssh-agent || ( apt-get update -yqq && apt-get install -yqq openssh-client)'
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'

# Deployment to staging environment of ELTE
# All pushes on develop
deploy_staging:
  extends: .deploy
  variables:
    DEPLOY_PATH: /var/www/tms_dev/backend-core/
  script:
    - ssh-add <(echo "$CD_PRIVATE_KEY")
    - |
      ssh -p22 gitlab-deployer@tms.inf.elte.hu << CMD
        set -e
        set -x
        umask 002
        cd $DEPLOY_PATH
        git status
        git pull
        composer install --prefer-dist --no-ansi --no-interaction
        ./yii migrate --interactive=0
      CMD
  environment:
    name: staging
    url: https://tms.inf.elte.hu/dev-api/
  rules:
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH && $CI_PROJECT_NAMESPACE == "tms-elte"'

# Deployment to production environment of ELTE
# All v* tags, excluding prereleases
deploy_production:
  extends: .deploy
  variables:
    DEPLOY_PATH: /var/www/tms_prod/backend-core/
  script:
    - ssh-add <(echo "$CD_PRIVATE_KEY")
    - |
      ssh -p22 gitlab-deployer@tms.inf.elte.hu << CMD
        set -e
        set -x
        umask 002
        cd $DEPLOY_PATH
        git status
        git fetch origin
        git checkout tags/$CI_COMMIT_TAG
        composer install --prefer-dist --no-ansi --no-interaction
        ./yii migrate --interactive=0
      CMD
  environment:
    name: production
    url: https://tms.inf.elte.hu/api/
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[^-].*$/ && $CI_PROJECT_NAMESPACE == "tms-elte"'

# Upload code coverage report to CodeCov
codecov:
  stage: deploy
  image: ubuntu:20.04
  before_script:
    - 'which curl || ( apt-get update -yqq && apt-get install -yqq curl)'
  script:
    - curl -Os https://uploader.codecov.io/latest/linux/codecov
    - chmod +x codecov
    - ./codecov -f tests/_output/coverage.xml -t ${CODECOV_TOKEN}

# Docker image build
.docker-login: &docker-login
  - docker version
  # Login to Docker Hub and GitLab Container Registry
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  - docker login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_TOKEN"

.docker:
  stage: deploy
  image: docker:latest
  services:
    - docker:dind
  variables:
    IMAGE_TAG: $CI_COMMIT_REF_NAME
  before_script:
    - *docker-login
  script:
    # Build Docker image
    - docker build . --pull -t "tmselte/backend-core:$IMAGE_TAG" -t "$CI_REGISTRY_IMAGE:$IMAGE_TAG"
    # Push Docker image to registries
    - docker push "$CI_REGISTRY_IMAGE:$IMAGE_TAG"
    - docker push "tmselte/backend-core:$IMAGE_TAG"

docker_latest:
  extends: .docker
  variables:
    IMAGE_TAG: latest
  rules:
    - if: $CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAMESPACE == "tms-elte"

docker_nightly:
  extends: .docker
  variables:
    IMAGE_TAG: nightly
  rules:
    - if: $CI_COMMIT_BRANCH == "develop" && $CI_PROJECT_NAMESPACE == "tms-elte"

docker_version:
  extends: .docker
  variables:
    IMAGE_TAG: $CI_COMMIT_TAG
  before_script:
    - *docker-login
    # Remove v prefix from tag
    - IMAGE_TAG=${IMAGE_TAG#v}
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_TAG =~ /^v[^-].*$/ && $CI_PROJECT_NAMESPACE == "tms-elte"'
